migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("cy9hbq6d67y5xb2")

  collection.listRule = ""
  collection.viewRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("cy9hbq6d67y5xb2")

  collection.listRule = null
  collection.viewRule = null

  return dao.saveCollection(collection)
})
