import { PrivateRoute } from '@/uikit/PrivateRoute.tsx';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { CartPanel } from './core/cart/CartPanel.tsx';
import { NavBar } from './core/NavBar.tsx';
import { CartPage, LoginPage, ShopPage } from './pages';
import { CheckoutPage } from './pages/cart/CheckoutPage.tsx';
import { CmsPage } from './pages/cms/CmsPage.tsx';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <CartPanel />

      <div className="page">
        <Routes>
          <Route path="shop" element={<ShopPage />} />
          <Route path="cart" element={<CartPage />} />
          <Route path="checkout" element={<CheckoutPage />} />
          <Route path="thankyou" element={<div>thank you</div>} />
          <Route path="login" element={<LoginPage />} />
          <Route path="cms" element={<PrivateRoute><CmsPage /></PrivateRoute>} />
          <Route path="*" element={<Navigate to="shop" />} />
        </Routes>
      </div>

    </BrowserRouter>
  )
}

export default App
