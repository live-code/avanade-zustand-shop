import { create } from 'zustand';

export interface CartPanelState {
  isOpened: boolean;
  open: () => void;
  close: () => void;
  toggle: () => void;
}

export const useCartPanel = create<CartPanelState>((set, get) => ({
  isOpened: false,
  open: () => set({ isOpened: true }),
  close: () => set({ isOpened: false }),
  toggle: () => set({ isOpened: !get().isOpened})
}))
