import { useNavigate } from 'react-router-dom';
import { selectCartList, selectTotalCartCost } from './cart.selectors.ts';
import { useCart } from './useCart.tsx';
import { useCartPanel } from './useCartPanel.ts';

export function CartPanel() {
  const isOpened = useCartPanel(state => state.isOpened)
  const closeCartPanel = useCartPanel(state => state.close)
  const list = useCart(selectCartList);
  const totalCost = useCart(selectTotalCartCost);

  const navigate = useNavigate();

  function gotoCart() {
    closeCartPanel();
    navigate('cart')
  }

  return isOpened ? (
    <div className="fixed bg-slate-800 text-white right-4 top-24 p-3 rounded-xl shadow-2xl w-96">
      <ul className="flex flex-col gap-4">
        {
         list.map((cartItem) => {
           return (
             <li key={cartItem.product.id} className="flex justify-between items-center border-b border-slate-600 pb-3">
               <div> {cartItem.product.name}</div>
               <div className="flex gap-3">
                 <div>({cartItem.qty} x € {cartItem.product.cost})</div>
                 <div>€  {cartItem.product.cost * cartItem.qty }</div>
               </div>
             </li>
           )
         })
        }
      </ul>

      <div className="flex justify-end text-xl font-bold my-3">
        Total: € {totalCost}
      </div>


      <div className="flex justify-center">
        <button className="btn primary" onClick={gotoCart}>Go to Cart</button>
      </div>
    </div>
  ) : null
}
