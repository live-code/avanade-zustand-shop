import { create } from 'zustand';
import { CartItem } from '../../model/cart-item.ts';
import { Product } from '../../model/product.ts';

export interface CartState {
  list: CartItem[];
  addToCart: (product: Product) => void;
  removeFromCart: (productId: string) => void;
  increaseQty: (productId: string) => void;
  decreaseQty: (productId: string) => void;
  clearCart: () => void;
}

export const useCart = create<CartState>((set, get) => ({
  list: [],
  addToCart: (product: Product) => {
    const found = get().list.find(item => item.product.id === product.id)

    if (found) {
      get().increaseQty(product.id);
    } else {
      // add item with qty = 1
      const cartItem: CartItem = { product, qty: 1 }
      set(state => ({ list: [...state.list, cartItem ]}))
    }

  },
  removeFromCart: (productId: string) => {
    set(state => ({ list: state.list.filter(u => u.product.id !== productId) }));
  },
  increaseQty: (productId: string) => {
    const found = get().list.find(item => item.product.id === productId);
    if (found) {
      found.qty++;

      set(state => ({
        list: state.list.map(item => {
          return item.product.id === found?.product.id ? found : item
        }),
      }))
    }
  },
  decreaseQty: (productId: string) => {
    const found = get().list.find(item => item.product.id === productId);
    // remove element if qty become zero
    if (found?.qty === 1) {
      get().removeFromCart(productId);
      return;
    }

    // decrease qty
    if (found && found.qty > 0) {
      found.qty--;
      set({
        list: get().list.map(item => {
          return item.product.id === found?.product.id ? found : item;
        })
      })
    }
  },
  clearCart: () => {
    set({ list: [] });
  },
}))
