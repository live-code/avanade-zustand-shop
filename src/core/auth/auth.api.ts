import { pb } from '@/core/api/pocketbase.ts';

export function login(email: string, password: string) {
  return pb.admins.authWithPassword(email, password)
}

export function logout() {
  return pb.authStore.clear();
}

export function getToken() {
  return pb.authStore.token;
}

export function isLogged() {
  return pb.authStore.isValid;
}
