import * as AuthApi from '@/core/auth/auth.api.ts';
import { create } from 'zustand';

export interface AuthState {
  token: string | null;
  isLogged: boolean;
  error: boolean;
  login: (email: string, pass: string) => void;
  logout: () => void;
}

export const useAuth = create<AuthState>((set) => ({
  token: AuthApi.getToken(),
  isLogged: AuthApi.isLogged(),
  error: false,
  login: (email: string, pass: string) => {

    set({error: false, isLogged: false, token: null});

     AuthApi.login(email, pass)
      .then(res => {
        console.log(res)
        set({
          isLogged: AuthApi.isLogged(),
          token: AuthApi.getToken()
        })
      })
      .catch(() => {
        set({ error: true })
      })



  },
  logout: () => {
    AuthApi.logout();
    set({ isLogged: false, token: null})
  }
}))
