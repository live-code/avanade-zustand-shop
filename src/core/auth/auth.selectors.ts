// services/auth/auth.selectors.ts
import { AuthState } from './useAuth';

export const selectAuthError = (state: AuthState) => state.error;
export const selectAuthToken = (state: AuthState) => state.token;
export const selectIsLogged = (state: AuthState) => state.isLogged;
