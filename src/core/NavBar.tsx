import { selectIsLogged } from '@/core/auth/auth.selectors.ts';
import { useAuth } from '@/core/auth/useAuth.ts';
import { IfLogged } from '@/uikit/IfLogged.tsx';
import { NavLink, useNavigate } from 'react-router-dom';
import logo from '../assets/laptop.png';
import { selectCartIsEmpty, selectTotalItems } from './cart/cart.selectors.ts';
import { useCart } from './cart/useCart.tsx';
import { useCartPanel } from './cart/useCartPanel.ts';

const activeCls = (obj: {isActive: boolean}) =>
  obj.isActive ? 'text-xl text-sky-400 font-bold' : 'text-xl text-white';


export function NavBar() {
  const toggle = useCartPanel(state => state.toggle);
  const totalItems = useCart(selectTotalItems)
  const isEmpty = useCart(selectCartIsEmpty)
  const logout = useAuth(state => state.logout)
  const isLogged = useAuth(selectIsLogged)
  const navigate = useNavigate();

  function logoutHandler() {
    logout()
    navigate('/login')
  }

  return (
    <div className="fixed z-10 top-0 left-0 right-0 shadow-2xl">
      <div className="flex items-center justify-between h-20 bg-slate-900 text-white p-3 shadow-2xl">
        {/*Logo*/}
        <div className="flex items-center gap-3">
          <img src={logo} alt="" className="w-16"/>
          <NavLink to="shop" className={activeCls}>FB SHOP</NavLink>
          <IfLogged
            else={<NavLink to="login" className={activeCls}>Login</NavLink>}
          >
            <div onClick={logoutHandler}>Logout</div>
          </IfLogged>
        </div>

        {/*Cart Button Badge */}
        {
          !isEmpty ? <div>
            <button className="btn accent lg" onClick={toggle}>
              Cart: {totalItems}
            </button>
          </div> : null
        }

      </div>

    </div>
  )
}
