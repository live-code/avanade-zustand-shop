import { selectIsLogged } from '@/core/auth/auth.selectors.ts';
import { useAuth } from '@/core/auth/useAuth.ts';
import React, { PropsWithChildren } from 'react';

interface IfLoggedProps {
  else?: React.ReactNode;
}

export function IfLogged(props: PropsWithChildren<IfLoggedProps>) {
  const isLogged = useAuth(selectIsLogged)

  return isLogged ?
    <>{props.children}</> :
    <>{props.else}</>

}
