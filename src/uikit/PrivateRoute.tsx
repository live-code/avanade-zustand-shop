import { selectIsLogged } from '@/core/auth/auth.selectors.ts';
import { useAuth } from '@/core/auth/useAuth.ts';
import { PropsWithChildren } from 'react';
import { Navigate } from 'react-router-dom';

export function PrivateRoute(props: PropsWithChildren) {
  const isLogged = useAuth(selectIsLogged)

  return isLogged ?
    <>{props.children}</> :
    <Navigate to="/login" />

}
