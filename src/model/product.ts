export interface Product {
  collectionId: string;
  collectionName: string;
  cost: number;
  created: string;
  data: string;
  id: string;
  img: string;
  name: string;
  updated: string;
}
