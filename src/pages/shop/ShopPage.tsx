import { pb, useCart, useCartPanel } from '@/core/index';
import { ServerError } from '@/uikit/ServerError.tsx';
import { Spinner } from '@/uikit/Spinner.tsx';
import { useEffect, useState } from 'react';
import { Product } from '../../model/product.ts';
import { ProductCard } from './components/ProductCard.tsx';


export function ShopPage() {
  const [products, setProducts] = useState<Product[]>([])
  const [pending, setPending] = useState(false);
  const [error, setError] = useState(false);
  const openCartPanel = useCartPanel(state => state.open)
  const closeCartPanel = useCartPanel(state => state.close)
  const addToCart = useCart(state => state.addToCart)

  useEffect(() => {
    closeCartPanel();
    setPending(true);
    pb.collection('products').getList<Product>()
      .then(res => {
        setProducts(res.items)
      })
      .catch(() => {
        setError(true)
      })
      .finally(() => {
        setPending(false)
      })
  }, [])

  function addToCartHandler(product: Product) {
    addToCart(product);
    openCartPanel()
  }

  return (
    <div>
      <h1 className="title">SHOP</h1>

      {pending && <Spinner />}
      {error && <ServerError />}

      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-2">
      {
        products.map(p =>
          <ProductCard
            product={p}
            key={p.id}
            onAddToCart={() => addToCartHandler(p)} />
        )
      }
      </div>

    </div>
  )
}

export function getImgPath(item: any) {
  return `http://127.0.0.1:8090/api/files/${item.collectionId}/${item.id}/${item.img}`
}
