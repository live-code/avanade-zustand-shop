import { Product } from '../../../model/product.ts';
import { getImgPath } from '../ShopPage.tsx';
import defaultImg from '../../../assets/laptop.png';

interface ProductCardProps {
  product: Product;
  onAddToCart: () => void;
}

export function ProductCard(props: ProductCardProps) {
  const { product } = props;

  return (
    <div className="bg-slate-300 rounded-xl shadow-xl p-3">

      <img src={product.img ? getImgPath(product) : defaultImg} alt=""/>

      <div className="flex justify-between p-3">
        <div className="text-xl">{product.name}</div>
        <div>€ {product.cost}</div>
      </div>

      <div className="flex justify-center">
        <button className="btn dark" onClick={props.onAddToCart}>ADD TO CART</button>
      </div>


    </div>
  )
}
