import { selectCartList, selectTotalCartCost } from '@/core/index';
import { useCart } from '@/core/cart/useCart.tsx';
import { NavLink } from 'react-router-dom';

export function CartPage() {

  const list = useCart(selectCartList);
  const increase = useCart(state => state.increaseQty);
  const decrease = useCart(state => state.decreaseQty);
  const removeFromCart = useCart(state => state.removeFromCart);
  const total = useCart(selectTotalCartCost);

  return (
    <div className="m-10">
      <h1 className="title">CART</h1>

      {
        list.map(item => {
          return (
            <div className="flex justify-between" key={item.product.id}>
              <div>{item.product.name}</div>

              <div className="flex gap-3 items-center">
                <i className="fa fa-minus-circle" onClick={() => decrease(item.product.id)}></i>
                qty: {item.qty}
                <i className="fa fa-plus-circle" onClick={() => increase(item.product.id)}></i>
              </div>
              <div className="flex gap-3 items-center">
                <div>€ {item.product.cost * item.qty}</div>
                <div>(unitario € {item.product.cost})</div>
                <i className="fa fa-trash"  onClick={() => removeFromCart(item.product.id)}></i>
              </div>
            </div>
          )
        })
      }
      <hr/>
       € {total}

      <br/>
      <br/>
      <br/>
      <NavLink to="/checkout" className="btn dark">Go to checkout</NavLink>

    </div>
  )
}
