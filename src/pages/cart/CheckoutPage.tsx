import { selectCartList, selectTotalCartCost } from '@/core/cart/cart.selectors.ts';
import { useCart } from '@/core/cart/useCart.tsx';
import clsx from 'clsx';
import * as React from 'react';
import { FormEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';

export function CheckoutPage() {
  const [user, setUser] = useState({ name: '', email: ''});
  const [dirty, setDirty] = useState(false)

  const clearCart = useCart(state => state.clearCart);
  const total = useCart(selectTotalCartCost);
  const order = useCart(selectCartList)
  const navigate = useNavigate();

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const key = e.currentTarget.name;
    const value = e.currentTarget.value;
    setUser(s => ({...s, [key]: value}))
    setDirty(true)
  }

  function confirmOrderHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const orderInfo = {
      user,
      order,
      total,
      status: 'pending'
    }

    // clear Cart
    clearCart();
    navigate('/thankyou')

    // redirect thank you
    console.log(orderInfo);
  }

  const isNameValid = user.name.length > 1;
  const isEmailValid = user.email.length > 1;
  const isValid = isNameValid && isEmailValid;

  return (
    <div className="max-w-sm mx-auto">
      <h1 className="title">CHECKOUT</h1>

      <div className="text-xl my-3 border-b">TOTAL: € {total}</div>

      <form className="flex flex-col gap-3" onSubmit={confirmOrderHandler}>
        Your name:
        <input type="text" className={clsx({'error': !isNameValid && dirty})} name="name" placeholder="your name" value={user.name} onChange={onChangeHandler}/>

        Your email
        <input type="email" className={clsx({'error': !isEmailValid && dirty})}  name="email" placeholder="Your email" value={user.email} onChange={onChangeHandler} />

        <button className="btn primary" disabled={!isValid}>
          CONFIRM ORDER
        </button>
      </form>
    </div>
  )
}
