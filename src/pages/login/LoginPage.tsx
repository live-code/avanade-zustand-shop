import { selectAuthError, selectIsLogged } from '@/core/auth/auth.selectors.ts';
import { useAuth } from '@/core/auth/useAuth.ts';
import { ServerError } from '@/uikit/ServerError.tsx';
import { ChangeEvent, FormEvent, useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

export function LoginPage() {
  const [formData, setFormData] = useState({ username: 'info@test.com', password: '1234567890'})

  const login = useAuth(state => state.login)
  const isLogged = useAuth(selectIsLogged)
  const error = useAuth(selectAuthError)
  const navigate = useNavigate()


  useEffect(() => {
    if (isLogged) {
      navigate('/cms')
    }
  }, [isLogged, navigate])

  function changeHandler(e: ChangeEvent<HTMLInputElement>) {
    const value = e.currentTarget.value;
    const name = e.currentTarget.name;
    setFormData(s => ({ ...s, [name]: value}))
  }

  const isValid = formData.username.length && formData.password.length


  async function doLogin(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    console.log(formData)

    login(formData.username, formData.password)
  }

  return !isLogged ? (
    <div className="page-sm">
      <h1 className="title">LOGIN</h1>

      {error && <ServerError />}

      <form onSubmit={doLogin} className=" flex flex-col gap-3">
        <input type="text" placeholder="username" name="username" value={formData.username} onChange={changeHandler} />
        <input type="password" placeholder="password" name="password" value={formData.password} onChange={changeHandler} />
        <button className="btn primary" type="submit" disabled={!isValid}>SIGN IN</button>
      </form>
    </div>
  ) : null
}
